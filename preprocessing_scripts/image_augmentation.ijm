// This ImageJ/Fiji macro rotates and flips images in a given directory. The script does not read subdirectories

// set your input directory below
input = "/path/to/input/directory/";

// set your output directory below
transformations = "/path/to/output/directory/";

nrotation = 3; // Set here the number of rotations that should be performed to the original image

degree = 90; // Set here the degree of rotation for each increment

F = 1; //Set if you wopuld also like to mirror the images. Set to 1 flip. Set to 0 skip image flipping.

// Below is the processing pipeline

function rot(input, transformations, filename) {

		open(input + filename);
		saveAs("Tiff", transformations + filename);
		
		if	(F==1) {
			run("Flip Horizontally", "stack");	
		 	saveAs("Tiff", transformations + filename + "_flip");
		 	run("Close");
		 	
		}
				
		if (nrotation>0) {
			
			for(x=1; x<=nrotation; x++) {

		        open(input + filename);
				run("Rotate... ", "angle=" + degree*x + " grid=0 interpolation=None stack");
				saveAs("Tiff", transformations + filename + "_rot_" + degree*x);
		
				if 	(F==1) {
				 	run("Flip Horizontally", "stack");	
				 	saveAs("Tiff", transformations + filename + "_rot_" + degree*x + "_flip");
				    }
			
			run("Close All");
			
			}
					
		} 	else {
						
			run("Close");
			
		}

}

list = getFileList(input);
for (i = 0; i < list.length; i++)
        rot(input, transformations, list[i]);
