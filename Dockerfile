FROM tboo/csbdeep_gpu_docker:latest

WORKDIR /data 

RUN wget --no-check-certificate --content-disposition "https://owncloud.gwdg.de/index.php/s/zmZlCkkMX4JXupC/download" &&\
    unzip Lumicare_sample_data.zip && rm Lumicare_sample_data.zip &&\
    chmod +x *py
