# This script requires a training data set in .npz format for network training

from __future__ import print_function, unicode_literals, absolute_import, division
import numpy as np

from tifffile import imread
from csbdeep.utils import axes_dict, plot_some, plot_history
from csbdeep.io import load_training_data
from csbdeep.models import Config, CARE

# Load the training data. make sure to define the right training data set. Define the percentage that is used for validation during training.

(X,Y), (X_val,Y_val), axes = load_training_data('train_data/patches_1s_to_1min.npz', validation_split=0.1, verbose=True)

c = axes_dict(axes)['C']
n_channel_in, n_channel_out = X.shape[c], Y.shape[c]

# Set the training parameters below. Reduce the batch size if you have large patches or limited GPU memory

config = Config(axes, n_channel_in, n_channel_out, train_epochs=150, train_steps_per_epoch=200, probabilistic=True, train_batch_size=32)
print(config)
vars(config)

# Train the model

model = CARE(config, 'model_1s_to_1min', basedir='models') # Define a name and the location where the model is saved

history = model.train(X,Y, validation_data=(X_val,Y_val))
