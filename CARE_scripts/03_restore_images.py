# This script uses a previsously trained CARE model to restrote a noisy input image set 

from __future__ import print_function, unicode_literals, absolute_import, division
import numpy as np
import os, sys

from tifffile import imread
from csbdeep.utils import Path, download_and_extract_zip_file, plot_some
from csbdeep.io import save_tiff_imagej_compatible
from csbdeep.models import CARE

# In this configuration, the script can loop through sequentially numbered images for batch restoration
# Here we assume images are named 0001_noisy.tif 0002_noisy.tif ...

# load the model - specify name and location

model = CARE(config=None, name='model_1s_to_1min', basedir='models')  

# Use the model for image restoration

for t in range (1, 2): # adjust number of loops. Current setting starts at image 0001 and stops before image 0002. e.g. only one image is processed

    t = str(t)
    x = imread('restore_data/noisy/' + t.zfill(4) + '_noisy.tif')

    axes = 'ZYX'
    print('image size =', x.shape)
    print('image axes =', axes)

    restored = model.predict(x, axes, n_tiles=(1,2,2))

    save_tiff_imagej_compatible('restore_data/restoration/' + t.zfill(4) + '_noisy_%s.tif' % model.name, restored, axes)
