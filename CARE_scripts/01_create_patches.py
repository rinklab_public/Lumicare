# This is an example script to generate training patches for CARE in headless mode

from __future__ import print_function, unicode_literals, absolute_import, division
import numpy as np

from tifffile import imread
from csbdeep.utils import download_and_extract_zip_file, plot_some
from csbdeep.data import RawData, create_patches

raw_data = RawData.from_folder (
    basepath    = 'train_data/raw_data',  # Set here the base directory which contains directories for noisy and groundtruth data
    source_dirs = ['1s'],                 # The name of the subdirectory that contains the noisy data
    target_dir  = '1min',                 # The name of the subdirectory that contains ground truth data
    axes        = 'ZYX',
)

X, Y, XY_axes = create_patches (
    raw_data            = raw_data,
    patch_size          = (16,64,64),                            # The dimensions of your training patches in ZYX
    n_patches_per_image = 200,                                   # Number of training patches that is sampled from each source stack
    save_file           = 'train_data/patches_1s_to_1min.npz',   # Set the filename for the training data set
)

assert X.shape == Y.shape
print("shape of X,Y =", X.shape)
print("axes  of X,Y =", XY_axes)
