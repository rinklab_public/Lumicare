# LumiCARE - accelerating luminescence imaging 

## Introduction

* We provide a framework that is intended to dramatically shorten exposure times in luminescence imaging. 
* Our pipeline is based on the previously [published](https://www.nature.com/articles/s41592-018-0216-7) CSBDeep package. Please cite it when denoising your luminescence data.
* In this repository we provide scripts that allow you to train networks and denoise images headless in a server environment. For graphical feedback and step by step introductions of CSBDeep please consult the [documentation](http://csbdeep.bioimagecomputing.com/doc/) and the respective GitHub [repository](https://github.com/CSBDeep/CSBDeep).

## Usage

* As LumiCARE is entirely based on CARE you can run it by following the installation instructions [here](http://csbdeep.bioimagecomputing.com/doc/install.html). 
* We also provide a generic [docker container](https://hub.docker.com/r/tboo/csbdeep_gpu_docker). Detailled installation and usage instructions are provided.
* If you would like to try our scripts within an isolated environment please use the Dockerfile in this repository to build a container that includes CARE, our framework scripts and sample data.
* To build the container, download the Dockerfile from this repository and run in the download directory from a terminal `docker build -t rinklab/lumicare:v0.1 .` to generate the container.  
* Our `CARE_scripts` work out of the box when the data is structured as shown below. Note, that corresponding noisy and ground truth images need to have the same name but are located in different directories. To customize the scripts consider the annotations in the respective files.

```
├── 01_create_patches.py
├── 02_train_network.py
├── 03_restore_images.py
├── models
├── restore_data
│   ├── ground_truth
│   │   └── 0001_ground_truth.tif
│   ├── noisy
│   │   └── 0001_noisy.tif
│   └── restoration
└── train_data
    └── raw_data
        ├── 1min
        │   ├── 001_Nanoluc.tif
        │   ├── 002_Nanoluc.tif
        │   ├── 003_Nanoluc.tif
        └── 1s
            ├── 001_Nanoluc.tif
            ├── 002_Nanoluc.tif
            ├── 003_Nanoluc.tif

```
* A sample data set that is structured as shown above can be downloaded [here](https://owncloud.gwdg.de/index.php/s/zmZlCkkMX4JXupC/download) or ships directly with the docker container from this repository

* CARE is generally divided into 3 processing steps: 

`01_create_patches.py` samples small training patches from the provided raw image pairs for subsequent network training. The size and number of these patches can be varied and is limited by the available memory.

`02_train_network.py` trains the network/model for denoising or upsampling your data. It requires the training patches generated with `01_create_patches.py`. Make sure that the script refers to the correct training patch file.

`03_restore_images.py` uses a previously trained model to denoise/upsample new data. Make sure to refer to the right model file.
